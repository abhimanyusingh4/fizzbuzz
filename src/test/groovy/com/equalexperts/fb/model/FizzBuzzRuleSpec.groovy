package com.equalexperts.fb.model

import spock.lang.Specification

class FizzBuzzRuleSpec extends  Specification {

    FizzBuzzRule fizzBuzzRule

    def setup(){
        fizzBuzzRule = new FizzBuzzRule();
    }

    def "apply: Should return fizz for number divisible by 3 only"(){
        given:
        Integer value = 3
        when:
        String result = fizzBuzzRule.apply(value)
        then:
        assert result!=null
        assert result == "fizz"
    }

    def "apply: Should return buzz for number divisible by 5 only"(){
        given:
        Integer value = 5
        when:
        String result = fizzBuzzRule.apply(value)
        then:
        assert result!=null
        assert result == "buzz"
    }

    def "apply: Should return fizzbuzz for number divisible by 3 and 5"(){
        given:
        Integer value = 15
        when:
        String result = fizzBuzzRule.apply(value)
        then:
        assert result!=null
        assert result == "fizzbuzz"
    }

    def "apply: Should return fizzbuzz for number not divisible by 3 and 5"(){
        given:
        Integer value = 2
        when:
        String result = fizzBuzzRule.apply(value)
        then:
        assert result!=null
        assert result == "2"
    }
}
