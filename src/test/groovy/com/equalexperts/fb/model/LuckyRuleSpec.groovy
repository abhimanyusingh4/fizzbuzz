package com.equalexperts.fb.model

import spock.lang.Specification

class LuckyRuleSpec extends Specification {
    LuckyRule luckyRule
    Rule lowPriorityRule

    def setup(){
        this.lowPriorityRule = new FizzBuzzRule()
        this.luckyRule = new LuckyRule(lowPriorityRule)
    }

    def "apply: Should return lucky if number contains 3"(){
        given:
        Integer value = 13
        when:
        String result = luckyRule.apply(value)
        then:
        result == "lucky"
    }

    def "apply: Should return result of low priority rule if number does not contain 3"(){
        given:
        Integer value = 12
        String resultOfLowPriority =  lowPriorityRule.apply(value)
        when:
        String result = luckyRule.apply(value)
        then:
        result == resultOfLowPriority
    }
}
