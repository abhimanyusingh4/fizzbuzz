package com.equalexperts.fb

import com.equalexperts.fb.model.FizzBuzzRule
import com.equalexperts.fb.model.LuckyRule
import spock.lang.Specification

import java.util.stream.IntStream

class FizzBuzzApplierSpec extends Specification {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream()
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream()
    private final PrintStream originalOut = System.out
    private final PrintStream originalErr = System.err

    def setup() {
        System.setOut(new PrintStream(outContent))
        System.setErr(new PrintStream(errContent))
    }

    def cleanup() {
        System.setOut(originalOut)
        System.setErr(originalErr)
    }

    def "applyRule: Should print fizz for number multiple of 3"() {
        given:
        def fizzBuzzApplier = new FizzBuzzApplier(new FizzBuzzRule())
        Integer value = 3
        when:
        fizzBuzzApplier.applyRules(value)
        then:
        assert outContent.toString() == "fizz "
    }


    def "applyRule: Should print fizz for number multiple of 5"() {
        given:
        def fizzBuzzApplier = new FizzBuzzApplier(new FizzBuzzRule())
        Integer value = 5
        when:
        fizzBuzzApplier.applyRules(value)
        then:
        assert outContent.toString() == "buzz "
    }

    def "applyRule: Should print fizz for number multiple of 3 and 5"() {
        given:
        def fizzBuzzApplier = new FizzBuzzApplier(new FizzBuzzRule())
        Integer value = 15
        when:
        fizzBuzzApplier.applyRules(value)
        then:
        assert outContent.toString() == "fizzbuzz "
    }

    def "applyRule: Should print number for number not multiple of 3 and 5 or both"() {
        given:
        def fizzBuzzApplier = new FizzBuzzApplier(new FizzBuzzRule())
        Integer value = 2
        when:
        fizzBuzzApplier.applyRules(value)
        then:
        assert outContent.toString() == "2 "
    }

    def "applyRule: Should print lucky for number containing 3"() {
        given:
        def fizzBuzzApplier = new FizzBuzzApplier(new LuckyRule(new FizzBuzzRule()))
        Integer value = 13
        when:
        fizzBuzzApplier.applyRules(value)
        then:
        assert outContent.toString() == "lucky "
    }

    def "printReport: Should print report that contains count of elemnents printer fizz, buzz, fizzbuzz, lucky or integer"() {
        given:
        def fizzBuzzApplier = new FizzBuzzApplier(new LuckyRule(new FizzBuzzRule()))
        when:
        System.setOut(originalOut)
        IntStream.range(1, 55).forEach { value -> fizzBuzzApplier.applyRules(value) }
        System.setOut(new PrintStream(outContent))
        fizzBuzzApplier.printReport()
        then:
        assert outContent.toString().contains("lucky : 15\n" +
                "integer : 20\n" +
                "fizz : 11\n" +
                "fizzbuzz : 2\n" +
                "buzz : 6")
    }

}
