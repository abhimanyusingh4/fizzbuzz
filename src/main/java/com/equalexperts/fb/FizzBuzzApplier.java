package com.equalexperts.fb;

import com.equalexperts.fb.model.Rule;

import java.util.HashMap;
import java.util.Map;

public class FizzBuzzApplier {
    Rule rule;

    private Map<String, Integer> report;

    public FizzBuzzApplier(Rule rule) {
        this.rule = rule;
        this.report = new HashMap<>();
        report.put("integer", 0);
        report.put("fizz", 0);
        report.put("buzz", 0);
        report.put("fizzbuzz", 0);
        report.put("lucky", 0);
    }

    public void applyRules(final Integer value) {
        Object result  = updateReport(rule.apply(value)) + " ";
        System.out.print(result);
    }

    private Object updateReport(Object result){
        if(result instanceof  Integer){
            report.put("integer", report.get("integer")+1);
        }else {
            report.put(result.toString(), report.get(result.toString())+1);
        }
        return result;
    }


    public void printReport(){
        for (String key : report.keySet()){
            System.out.println(key + " : "+ report.get(key));
        }
    }

}
