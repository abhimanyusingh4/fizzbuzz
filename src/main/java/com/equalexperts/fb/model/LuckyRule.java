package com.equalexperts.fb.model;

public class LuckyRule implements Rule {
    private Rule lowPriorityRule;

    public LuckyRule(Rule lowPriorityRule) {
        this.lowPriorityRule = lowPriorityRule;
    }

    @Override
    public Object apply(Integer value) {
        Object result = lowPriorityRule!=null?lowPriorityRule.apply(value):value;
        if (containsThree(value)) {
            return "lucky";
        }
        return result;
    }

    private boolean containsThree(Integer value) {
        while (value != 0) {
            if (value % 10 == 3) {
                return true;
            } else {
                value /= 10;
            }
        }
        return false;
    }
}
