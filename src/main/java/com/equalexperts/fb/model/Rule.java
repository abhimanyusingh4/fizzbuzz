package com.equalexperts.fb.model;

public interface Rule {
    Object apply(Integer value);
}
