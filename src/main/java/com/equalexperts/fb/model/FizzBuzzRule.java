package com.equalexperts.fb.model;

public class FizzBuzzRule implements Rule {


    public Object apply(Integer value) {
        StringBuilder result = new StringBuilder();
        if (value % 3 == 0) {
            result.append("fizz");
        }
        if (value % 5 == 0) {
            result.append("buzz");
        }
        return result.length() != 0 ? result : value;
    }
}
